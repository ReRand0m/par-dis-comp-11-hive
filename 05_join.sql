USE velkerr_test;
SET hive.auto.convert.join=false;

SELECT /*+ MAPJOIN(ipregions) */ * FROM logsnonpart LEFT JOIN ipregions ON logs.ip = ipregions.ip
LIMIT 10;