add jar target/hive-1.0-SNAPSHOT.jar;

USE ${env:USER}_test;

create temporary function identity as 'org.example.IdentityUDF';

select identity(ip)
from Subnets
limit 10;