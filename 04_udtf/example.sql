add jar target/hive-1.0-SNAPSHOT.jar;

USE ${env:USER}_test;

create temporary function octets as 'org.example.OctetsUDTF';

select octets(ip)
from Subnets
limit 10;