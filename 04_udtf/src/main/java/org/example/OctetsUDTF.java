package org.example;

import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDTF;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspectorFactory;
import org.apache.hadoop.hive.serde2.objectinspector.StructObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.StringObjectInspector;

import java.util.ArrayList;
import java.util.List;

public class OctetsUDTF extends GenericUDTF {

    private StringObjectInspector address;
    private Object[] forwardObjArray = new Object[1];

    @Override
    public StructObjectInspector initialize(ObjectInspector[] args) throws UDFArgumentException {
        if(args.length != 1){
            throw new UDFArgumentException(getClass().getSimpleName() + " takes only 1 argument!");
        }
        address = (StringObjectInspector) args[0];

        final List<String> fieldNames = new ArrayList<String>(){{
            add("Octets");}};
        final List<ObjectInspector> fieldInspectors = new ArrayList<ObjectInspector>(){{
            add(PrimitiveObjectInspectorFactory.javaIntObjectInspector);
        }};
        return ObjectInspectorFactory.getStandardStructObjectInspector(fieldNames, fieldInspectors);
    }

    @Override
    public void process(Object[] objects) throws HiveException {
        for (String octet: address.getPrimitiveJavaObject(objects[0]).split("\\.")) {
            forwardObjArray[0] = Integer.parseInt(octet);
            forward(forwardObjArray);
        }
    }

    @Override
    public void close() throws HiveException {
    }
}