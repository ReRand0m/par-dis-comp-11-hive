add jar target/hive-1.0-SNAPSHOT.jar;

USE ${env:USER}_test;

create temporary function modify as 'org.example.ModifyUDF';

select modify(ip)
from Subnets
limit 10;