package org.example;

import org.apache.hadoop.hive.ql.exec.UDF;

public class ModifyUDF extends UDF {
    public int evaluate(String str) {
        int sum = 0;
        for (String octet: str.split("\\.")) {
            sum += Integer.parseInt(octet);
        }
        return sum;
    }
}