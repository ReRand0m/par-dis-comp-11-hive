add jar target/hive-1.0-SNAPSHOT.jar;

USE ${env:USER}_test;

create temporary function copyip as 'org.example.CopyIpUDTF';

select copyip(ip)
from Subnets
limit 10;